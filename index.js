const getCube = 2 ** 3;
const message = `The cube of 2 is ${getCube}.`

console.log(message)

const address = ["258 Washington Ave", "NW, California", 90011]
const [street, state, zipCode] = address;

console.log(`I live at ${street} ${state} ${zipCode}`)

const animal = ["Lolong","saltwater crocodile", "1075 kgs","20 ft 3in"]
const [name, type, weight, measurement] = animal;

console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}`)

let numbers = [1,2,3,4,5]

numbers.forEach((number) => console.log (`${number}`));

const reduceNumber = numbers.reduce((x,y) => x + y);

console.log(reduceNumber)

class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog)
